# certbot-host-europe-sshfs-telegram

Create/renew a SSL certificate/key Pair with SSHfs webroot on Host Europe sent through telegram



![](./HE-SSLex.png)

## INSTALLATION

* #### get the repo e.g. `git clone https://gitlab.com/the-foundation/certbot-host-europe-sshfs-telegram.git /etc/custom-renew-yourdomain.lan`
* #### create a domain.conf file IN the directory you just created
* #### install `telegram-notify`
* #### create `/etc/telegram-notify.letsencrypt.conf`

---
##### Example /etc/telegram-notify.letsencrypt.conf

```
[general]
api-key=1111111111:AABBCCDDEEFF0011233
user-id=-987654321
[network]
socks-proxy=
```
##### Example domain.conf:
```
HE_ID=12345676
HE_PATH=www/mydomain.com
TARGET_MAIL=default-mail-not-set@using-fallback-default.slmail.me
CERT_NAME=yourdomain.lan
TARGET_DOMAINS=yourdomain.lan,target.yourdomain.lan
```

## USAGE
### `run /bin/bash /path/to/he-renew-telegram.sh` after generating config files ,
#### e.g. by cron:
#### `42 23 * * * /bin/bash /path/to/he-renew-telegram.sh &>/dev/null`
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/certbot-host-europe-sshfs-telegram/README.md/logo.jpg" width="480" height="270"/></div></a>
