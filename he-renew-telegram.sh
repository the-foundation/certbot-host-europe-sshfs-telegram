#!/bin/bash


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
source ${SCRIPTPATH}/domain.conf
#CREATE A FILE IN SAME FOLDER NAMED domain.conf
#WITH FOLLOWING CONTENT ( example values filled ):

#HE_ID=12345676
#HE_PATH=www/mydomain.com
#TARGET_MAIL=default-mail-not-set@using-fallback-default.slmail.me
#CERT_NAME=yourdomain.lan
#TARGET_DOMAINS=yourdomain.lan,target.yourdomain.lan
[[ -z "$TARGET_MAIL" ]] && TARGET_MAIL=default-mail-not-set@using-fallback-default.slmail.me

## the script will connect with sshfs ( so it needs the public key installed in .ssh/authorized keys or ask( not preferable))
## validation will take place with the webroot method

_ssl_host_enddate_days() {
    target=$1
    end="$(echo | openssl s_client -connect "$target" 2>/dev/null |openssl x509 -enddate -noout  | sed -e 's#notAfter=##'|awk '{ printf "%04d-%02d-%02d\n", $4,(index("JanFebMarAprMayJunJulAugSepOctNovDec",$1)+2)/3, $2}')";
    #echo $end
    date_diff=$(( ($(date -d "$end" +%s) - $(LC_ALL=C date -d "$(LC_ALL=C date +%Y-%m-%d)" +%s) )/(60*60*24) ));printf '%s: %s' "$date_diff" "$target" ; }



examplepng=${SCRIPTPATH}/HE-SSLex.png

sums_before=$(md5sum /etc/letsencrypt/live/${CERT_NAME}/{fullchain.pem,privkey.pem})
test -e /tmp/.HostEuropeCertbot ||mkdir  /tmp/.HostEuropeCertbot ; chattr +i /tmp/.HostEuropeCertbot
sshfs -o allow_other,ServerAliveInterval=45,ServerAliveCountMax=2,reconnect,noatime wp${HE_ID}@wp${HE_ID}.server-he.de:  /tmp/.HostEuropeCertbot
##--preferred-challenges http-01,dns-01 --rsa-key-size 4096 
##DANGER##TEST
#(sleep 1;echo 1;sleep 0.5;echo  )|/usr/bin/certbot certonly --cert-name="${CERT_NAME}" -d "${TARGET_DOMAINS}" --manual-public-ip-logging-ok   --webroot -w /tmp/.HostEuropeCertbot/${HE_PATH}/ --email ${TARGET_MAIL}  --agree-tos  --rsa-key-size 4096  --staging --test-cert --break-my-certs  2>&1 >> /var/log/certbot-external-hosteurope.log
##DANGER##TEST

test -e /tmp/.HostEuropeCertbot/${HE_PATH}/||mkdir /tmp/.HostEuropeCertbot/${HE_PATH}/ 
results=$((sleep 1;echo 1;sleep 0.5;echo  )|/usr/bin/certbot certonly --cert-name="${CERT_NAME}" -d "${TARGET_DOMAINS}" --manual-public-ip-logging-ok    --webroot -w /tmp/.HostEuropeCertbot/${HE_PATH}/ --email ${TARGET_MAIL}  --agree-tos  --rsa-key-size 4096   2>&1 | tee -a /var/log/certbot-external-hosteurope.log)

#echo "$results"

#/usr/bin/certbot -d "${TARGET_DOMAINS}" --manual-public-ip-logging-ok  --test-cert  --webroot -w /tmp/.HostEuropeCertbot/${HE_PATH}/   certonly --staging  --email ${TARGET_MAIL}  --agree-tos  --rsa-key-size 4096                  &>/var/log/certbot-external-hosteurope.log
sums_after=$(md5sum /etc/letsencrypt/live/${CERT_NAME}/{fullchain.pem,privkey.pem})
notify=false
reason=""
## send notification if certs changed due to our renewal
[[ "${sums_after}" =  "${sums_before}" ]] || { reason=${reason}" "sums;notify=true ; } ;
## send notification if curl dilikes the current validity in general
curl --fail https://${CERT_NAME} &>/dev/null || { reason=${reason}" "curl_fail;notify=true ; } ;
curl --fail https://${CERT_NAME}  -o /dev/null -kLv 2>&1 |  grep "SSL certificate verify ok" -q || { reason=${reason}" "curl_ssl_verify;notify=true ; } ;
## send notification if certbot says cert expired
curl --fail https://${CERT_NAME}  -o /dev/null -kLv 2>&1 |  grep -i ertificate |grep -q -i expired  && { reason=${reason}" "curl_ssl_expired;notify=true ; } ;
## send notification if the certificate is subject to  renewal (20 days or less remaining)
[[ 21 -gt "$(_ssl_host_enddate_days ${CERT_NAME}:443|cut -d":" -f1)" ]] && { reason=${reason}" cert_validity:"$(_ssl_host_enddate_days ${CERT_NAME}:443);notify=true ; } ;

if [ "${notify}" =  "true" ] ; then

test -e /etc/letsencrypt/live/${CERT_NAME}/privkey.pem && test -e  /etc/letsencrypt/live/${CERT_NAME}/fullchain.pem && {

cat /etc/letsencrypt/live/${CERT_NAME}/privkey.pem > /tmp/${CERT_NAME}_key.key
cat /etc/letsencrypt/live/${CERT_NAME}/fullchain.pem > /tmp/${CERT_NAME}_certificate.pem
cd /tmp
tar cvzf HostEurope_SSL_for_${certname}_$(date +%F).tar.gz ${CERT_NAME}_key.key ${CERT_NAME}_certificate.pem &>/dev/null

MSGTXT=$reason" "'

 **SSL RENEW**
Please add the following files to '${CERT_NAME}' AND/OR '${TARGET_DOMAINS}' in your hosting panel:

- go to  "product admin" -> "webserver" -> "security & ssl"

( direct link: https://kis.hosteurope.de/administration/webhosting/admin.php?menu=6&mode=ssl_list&wp_id='$HE_ID' )

- click on "replace" next to '${CERT_NAME}'

- use the certificate file for CA as well



Regards'
IMGPARAM=""
test -e ${examplepng} && IMGPARAM="--photo ${examplepng}"
[[ -z "$IMGPARM" ]] && ( echo "Renewal example image:"| /usr/bin/telegram-notify ${IMGPARAM} --config /etc/telegram-notify.letsencrypt.conf   --text - 2>&1 ) |grep -v '[Info]'
( echo "$MSGTXT" |/usr/bin/telegram-notify  --config /etc/telegram-notify.letsencrypt.conf  --warning --document HostEurope_SSL_for_${certname}_$(date +%F).tar.gz  --text - 2>&1 ) |grep -v '[Info]'

rm  HostEurope_SSL_for_${certname}_$(date +%F).tar.gz ${CERT_NAME}_key.key ${CERT_NAME}_certificate.pem  /tmp/HostEurope_SSL_for_${certname}_$(date +%F).tar.gz 2>/dev/null
echo ; } ;

else
  date -u > /dev/shm/letsencrypt.${CERT_NAME}.OK
fi
fusermount -u /tmp/.HostEuropeCertbot
